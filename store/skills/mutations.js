import { SET_SKILLS } from './mutationTypes'

export default {
    [SET_SKILLS](state, payload) {
        state.skills = payload
    },
}