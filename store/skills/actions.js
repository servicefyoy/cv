import { SET_SKILLS } from './mutationTypes'

export default {
    async fetchSkills({ commit }, context) {
        return new Promise((resolve, reject) => {
            const skills = [
                {
                  name: 'Vuejs/Nuxt',
                  score: 4,
                },
                {
                  name: 'Nativescript',
                  score: 2,
                },
                {
                  name: 'Javascript',
                  score: 4,
                },
                {
                  name: 'HTML5',
                  score: 5,
                },
                {
                  name: 'SCSS',
                  score: 4.6,
                },
                {
                  name: 'React',
                  score: 2,
                },
                {
                  name: 'jQuery',
                  score: 4
                },
                {
                  name: 'Angular',
                  score: 2
                },
                {
                  name: 'PHP',
                  score: 3
                },
                {
                  name: 'Flash AS2/AS3',
                  score: 4.6
                },
                {
                  name: 'Googling :)',
                  score: 4.9
                },
                {
                  name: 'The project management',
                  score: 3.8
                }
            ]
            commit(SET_SKILLS, skills)
            resolve()
        })
    },
}