import { SET_COMPANIES } from './mutationTypes'

export default {
    [SET_COMPANIES](state, payload) {
        state.companies = payload
    },
}