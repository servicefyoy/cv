import { SET_COMPANIES } from './mutationTypes'

export default {
    async fetchCompanies({ commit }, context) {
        return new Promise((resolve, reject) => {
            const companies = [
              {
                time: '2018',
                name:'BB Finance Group',
                description: 'Development of a platform for websites (for faster page setup).',
                responsibility:'Technical side of the pages (entire website health)',
                technologies: ['Vuejs', 'Webback', 'Nuxtjs', 'scss', 'html5', 'javascript', 'nodejs', 'git', 'jQuery', 'UI - bootstrap','UI - bootstrap-vue']
              },
              {
                time: '2015',
                name: 'Deltabid',
                description: 'RFQ management tool developing - website, web application (node js)',
                responsibility:'Website and a specific part of the program',
                technologies: ['html5', 'javascript', 'php', 'nodejs', 'bower', 'git', 'angularjs', 'jQuery', 'gulp', 'sass', 'UI - zurb foundation']
              },
              {
                time: '2010',
                name: 'Telia',
                description: '2 last website development, developing of test environment (for development of webpage)',
                responsibility:'Telia webpage',
                technologies: ['UI - Bootstrap', 'jQuery', 'Bower', 'Grunt', 'node.js', 'javascript', 'html5', 'sass', 'photoshop']
              },
              {
                time: '2009',
                name: 'Swedbank',
                description: 'Development of web components (html, js, css). Business customer prototype development (php, html, css, js)',
                responsibility:'Veb components',
                technologies: ['javascript', 'html', 'css']
              },
              {
                time: '2006',
                name: '3DStuudio',
                description: 'CMS development, website development and implementation of CMS (php, html, css, js). Flash game development (as2, as3) and project management',
                responsibility:'Customer websites, CMS support',
                technologies: ['php', 'html', 'css', 'javascript', 'jquery', 'as2', 'as3', 'photoshop']
              },
              {
                time: '2003',
                name: 'DF',
                description: 'Website building (HTML, css, js, flash). Video editing (Adobe Premiere, Adobe After Effects), building of Microsoft word & excel templates (VBscript)',
                responsibility:'',
                technologies: ['html', 'css', 'javascript', 'as2', 'as3', 'vbscript', 'Adobe After Effect', 'Adobe Premiere']
              },
              {
                time: '2002',
                name: 'Freelancer',
                description: 'Website building, game building, project management',
                responsibility:'All projects',
                technologies: ['html', 'css', 'javascript', 'flash']
              },
              {
                time: '2000',
                name: 'Tele2',
                description: 'Daily maintenance of the Everyday.com portal - building and editing content pages (HTML, css, js)',
                responsibility:'Everyday.com portal',
                technologies: ['html', 'css', 'javascript']
              }
            ]
            commit(SET_COMPANIES, companies)
            resolve()
        })
    },
}