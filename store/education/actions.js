import { SET_EDUCATION } from './mutationTypes'

export default {
    async fetchEducation({ commit }, context) {
        return new Promise((resolve, reject) => {
            const education = [
              {
                time: '2004',
                name:'IT-Kolledž',
                description: 'Flash Advanced Flash ActionScript course'
              },
              {
                time: '1999 - 2000',
                name:'Tallinna Transpordikool',
                description: 'Informatics, basic knowledge of informatics (software, hardware, css, html, javascript, MS office)'
              },
              {
                time: '1998 - 1999',
                name:"The People's University of Helnaese, Danish",
                description: 'Danish culture, Danish history'
              },
              {
                time: '1995 - 1998',
                name:'Leppävirta Ammatikoulu, Finland',
                description: 'Hotel management'
              },
              {
                time: '1995',
                name:'I am 18!',
                description: 'Hallo world! :)'
              },
              {
                time: '1993 - 1995',
                name:'Orissaare High School, Saaremaa',
                description: 'high school'
              },
            ]
            commit(SET_EDUCATION, education)
            resolve()
        })
    },
}