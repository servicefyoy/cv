import { SET_EDUCATION } from './mutationTypes'

export default {
    [SET_EDUCATION](state, payload) {
        state.education = payload
    },
}