export default {
    getKnowledge: (state) => state.knowledge,
    getBestFriends: (state) => state.bestFriends,
    getFriends: (state) => state.friends,
    getConfusingThings: (state) => state.confusingThings,
    getLanguageSkills: (state) => state.languageSkills
}