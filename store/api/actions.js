import { SET_KNOWLEDGE, SET_BEST_FRIENDS, SET_FRIENDS, SET_CONFUSING_THINGS, SET_LANGUAGE_SKILLS } from './mutationTypes'

export default {
  async fetchKnowledge({ commit }, context) {
      return new Promise((resolve, reject) => {
          const knowledge = [
            'Git', 
            'node.js', 
            'Webpack', 
            'Grunt', 
            'Bower', 
            'Gimp', 
            'Laravel', 
            'Photoshop', 
            'Google polymer', 
            'UX/UI', 
            'TypeScript',
            'npm',
            'yarn',
            '...'
          ]
          
          commit(SET_KNOWLEDGE, knowledge)
          resolve()
      })
    },
    async fetchBestFriends({ commit }, context) {
      return new Promise((resolve, reject) => {
        const bestFriends = [
          'Nuxtjs',
          'Vuejs',
          'ES6',
          'Terminal', 
          'Visual Studio Code', 
          'Git', 
          'MacBook Pro',
          'Google Chrome', 
          'Firefox', 
          'Developer tool',
          'Bootstrap-vue'
        ]
        
        commit(SET_BEST_FRIENDS, bestFriends)
        resolve()
      })
    },
    async fetchFriends({ commit }, context) {
      return new Promise((resolve, reject) => {
          const friends = [
            'Terminal', 
            'Visual Studio Code', 
            'Git', 
            'MacBook Pro',
            'Google Chrome', 
            'Firefox', 
            'Gimp',
            'Developer tool',
          ]
          
          commit(SET_FRIENDS, friends)
          resolve()
      })
    },
    async fetchConfusingThings({ commit }, context) {
      return new Promise((resolve, reject) => {
          const confusingThings = [
            'ie', 
            'Paint',
          ]
          
          commit(SET_CONFUSING_THINGS, confusingThings)
          resolve()
      })
    },

    async fetchLanguageKills({ commit }, context) {
      return new Promise((resolve, reject) => {
          const languageSkills = [
            {
              name:'Estonian',
              score: 5,
            },
            {
              name:'Finnish',
              score: 3.5,
            },
            {
              name:'English',
              score: 3.5
            },
            {
              name:'Russian',
              score: 2.5
            },
            {
              name:'Danish ',
              score: 1
            },
          ]
          
          commit(SET_LANGUAGE_SKILLS, languageSkills)
          resolve()
      })
    },
}


