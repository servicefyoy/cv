import { SET_KNOWLEDGE, SET_BEST_FRIENDS, SET_FRIENDS, SET_CONFUSING_THINGS, SET_LANGUAGE_SKILLS } from './mutationTypes'

export default {
    [SET_KNOWLEDGE](state, payload) {
        state.knowledge = payload
    },
    [SET_BEST_FRIENDS](state, payload) {
        state.bestFriends = payload
    },
    [SET_FRIENDS](state, payload) {
        state.friends = payload
    },
    [SET_CONFUSING_THINGS](state, payload) {
        state.confusingThings = payload
    },
    [SET_LANGUAGE_SKILLS](state, payload) {
        state.languageSkills = payload
    }
    
}