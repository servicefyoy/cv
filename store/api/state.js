export default () => ({
    knowledge: [],
    bestFriends: [],
    friends: [],
    confusingThings: [],
    languageSkills: []
})